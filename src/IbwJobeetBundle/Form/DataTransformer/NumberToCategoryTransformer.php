<?php

namespace IbwJobeetBundle\Form\DataTransformer;

use IbwJobeetBundle\Entity\Category;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class NumberToCategoryTransformer implements DataTransformerInterface
{
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }


    /**
     * Transforms a string (number) to an object (category).
     *
     * @param  string $categoryNumber
     * @return Category|null
     * @throws TransformationFailedException if object (category) is not found.
     */
    public function transform($categoryNumber)
    {
// no category number? It's optional, so that's ok
        if (!$categoryNumber) {
            return 0;
        }

        $category = $this->manager
            ->getRepository('IbwJobeetBundle:Category')
// query for the category with this id
            ->find($categoryNumber);

        if (null === $category) {
// causes a validation error
// this message is not shown to the user
// see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'A category with number "%s" does not exist!',
                $categoryNumber
            ));
        }

        return $category;
    }

    /**
     * Transforms an object (category) to a string (number).
     *
     * @param  Category|null $category
     * @return string
     */
    public function reverseTransform($category)
    {
        if (null === $category) {
            return '';
        }

        return $category->getId();
    }
}
