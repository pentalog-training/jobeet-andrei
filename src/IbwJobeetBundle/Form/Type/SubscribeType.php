<?php
namespace IbwJobeetBundle\Form\Type;

use IbwJobeetBundle\Entity\Subscribe;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SubscribeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('categories', null, array('expanded' => true))
            ->add('periodicity', ChoiceType::class, array(
                'choices' => Subscribe::getPeriodicityValues()))
            ->add('submit', 'submit', array(
                'attr' => array('class' => 'btn btn-default pull-right')));

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IbwJobeetBundle\Entity\Subscribe',
        ));

    }

    public function getName()
    {
        return 'subscribe';
    }
}
