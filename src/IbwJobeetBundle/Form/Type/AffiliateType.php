<?php
namespace IbwJobeetBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AffiliateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('url')
            ->add('email')
            ->add('categories', null, array('expanded' => true))
            ->add('submit', 'submit', array(
                'attr' => array('class' => 'btn btn-default pull-right')));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IbwJobeetBundle\Entity\Affiliate',
        ));

    }

    public function getName()
    {
        return 'affiliate';
    }
}
