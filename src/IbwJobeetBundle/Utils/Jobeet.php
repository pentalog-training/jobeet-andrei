<?php
namespace IbwJobeetBundle\Utils;

class Jobeet
{
    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('/\P{L}+/', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
        if (function_exists('iconv')) {
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        }

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        // $text = preg_replace('#[^-w]+#', '', $text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;

//        // replace non letter of digits by -
//        $text = preg_replace('#[^\pLd]+#u', '-', $text);
//
//        // trim
//        if (function_exists('iconv')) {
//            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
//        }
//
//        // lowercase
//        $text = strtolower($text);
//
//        // remove unwanted characters
//        $text = preg_replace('#[^-w]+#', '', $text);
//
//        if (empty($text)) {
//            return 'n-a';
//        }
//
//        return $text;
    }
}
