<?php

namespace IbwJobeetBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class CategoryRepository
 * @package IbwJobeetBundle\Repository
 * 
 * @method findOneBySlug($slug)
 */

class CategoryRepository extends EntityRepository
{
    /**
     * @return \IbwJobeetBundle\Entity\Category|array
     */
    public function getWithJobs()
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT c FROM IbwJobeetBundle:Category c LEFT JOIN c.jobs j WHERE j.expiresAt > :date AND j.active = :activated')
            ->setParameter('date', date('Y-m-d H:i:s', time()))
            ->setParameter('activated', 1);

        return $query->getResult();
    }

    /**
     * @param \DateTime $fromDate
     * @return \IbwJobeetBundle\Entity\Category[]
     */
    public function getCategoriesForNewsletter($fromDate)
    {
        $query = $this->createQueryBuilder('c')
            ->leftJoin('c.jobs', 'j')
            ->where('j.createdAt >= :fromDate')
            ->orWhere('j.updatedAt >= :fromDate')
            ->setParameter('fromDate', $fromDate)
            ->getQuery();

        return $query->getResult();
    }

    public function getCategories()
    {
        $categories = $this->findAll();

        return $categories;
    }
}
