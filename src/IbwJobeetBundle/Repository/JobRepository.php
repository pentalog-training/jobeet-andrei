<?php

namespace IbwJobeetBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

/**
 * Class JobRepository
 * @package \IbwJobeetBundle\Repository
 * @method Job findOneByToken(string $token)
 */
class JobRepository extends EntityRepository
{
    /**
     * @param null $category_id
     * @param null $max
     * @param null $offset
     * @param null $affiliate_id
     * @return array
     */
    public function getActiveJobs($category_id = null, $max = null, $offset = null, $affiliate_id = null)
    {
        $qb = $this->createQueryBuilder('j')
            ->where('j.expiresAt > :date')
            ->setParameter('date', date('Y-m-d H:i:s', time()))
            ->andWhere('j.active = :activated')
            ->setParameter('activated', 1)
            ->orderBy('j.expiresAt', 'DESC');

        if($max) {
            $qb->setMaxResults($max);
        }

        if($offset) {
            $qb->setFirstResult($offset);
        }

        if($category_id) {
            $qb->andWhere('j.category = :category_id')
                ->setParameter('category_id', $category_id);
        }
        // j.category c, c.affiliate a
        if($affiliate_id) {
            $qb->leftJoin('j.category', 'c') 
                ->leftJoin('c.affiliates', 'a')
                ->andWhere('a.id = :affiliate_id')
                ->setParameter('affiliate_id', $affiliate_id)
            ;
        }

        $query = $qb->getQuery();

        return $query->getResult();
    }

    public function countActiveJobs($category_id = null)
    {
        $qb = $this->createQueryBuilder('j')
            ->select('count(j.id)')
            ->where('j.expiresAt > :date')
            ->setParameter('date', date('Y-m-d H:i:s', time()))
            ->andWhere('j.active = :activated')
            ->setParameter('activated', 1);

        if($category_id) {
            $qb->andWhere('j.category = :category_id')
                ->setParameter('category_id', $category_id);
        }

        $query = $qb->getQuery();

        return $query->getSingleScalarResult();
    }

    public function getActiveJob($id)
    {
        $query = $this->createQueryBuilder('j')
            ->where('j.id = :id')
            ->setParameter('id', $id)
            ->andWhere('j.expiresAt > :date')
            ->setParameter('date', date('Y-m-d H:i:s', time()))
            ->andWhere('j.active = :activated')
            ->setParameter('activated', 1)
            ->setMaxResults(1)
            ->getQuery();

        try {
            $job = $query->getSingleResult();
        } catch (NoResultException $e) {
            $job = null;
        }

        return $job;
    }

    public function getLatestPost($category_id = null)
    {
        $query = $this->createQueryBuilder('j')
            ->where('j.expiresAt > :date')
            ->setParameter('date', date('Y-m-d H:i:s', time()))
            ->andWhere('j.active = :activated')
            ->setParameter('activated', 1)
            ->orderBy('j.expiresAt', 'DESC')
            ->setMaxResults(1);

        if($category_id) {
            $query->andWhere('j.category = :category_id')
                ->setParameter('category_id', $category_id);
        }

        try{
            $job = $query->getQuery()->getSingleResult();
        } catch(\Doctrine\ORM\NoResultException $e){
            $job = null;
        }

        return $job;
    }

    public function cleanup($days)
    {
        $query = $this->createQueryBuilder('j')
            ->delete()
            ->where('j.active IS NULL')
            ->andWhere('j.created_at < :created_at')
            ->setParameter('created_at',  date('Y-m-d', time() - 86400 * $days))
            ->getQuery();

        return $query->execute();
    }

    /**
     * @param $query
     * @return \Doctrine\ORM\Query
     */
    public function searchForJob($query)
    {
        $qb = $this->createQueryBuilder('j')
            ->where('j.position LIKE :query')
            ->orWhere('j.type LIKE :query')
            ->orWhere('j.company LIKE :query')
            ->orWhere('j.location LIKE :query')
            ->leftJoin('j.category', 'c')
            ->orWhere('c.name LIKE :query')
            ->setParameter('query', '%' . $query . '%')
            ->andWhere('j.active = :active')
            ->setParameter('active', 1);

        return $qb->getQuery();
    }

    /**
     * @param \DateTime $fromDate
     */
    public function getLastNJobs($fromDate, $category, $n = 10)
    {
        $query = $this->createQueryBuilder('j')
            ->where('j.createdAt > :fromDate')
            ->orWhere('j.updatedAt > :fromDate')
            ->setParameter('fromDate', $fromDate)
            ->andWhere('j.category = :category')
            ->setParameter('category', $category)
            ->orderBy('j.updatedAt', 'DESC')
            ->setMaxResults($n)
            ->getQuery();
        
        return $query->getResult();
    }
}
