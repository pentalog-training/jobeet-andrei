<?php
namespace IbwJobeetBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Subscribe entity.
 *
 * @ORM\Entity(repositoryClass="IbwJobeetBundle\Repository\SubscribeRepository")
 * @ORM\Table(name="subscribe")
 * @ORM\HasLifecycleCallbacks()
 *
 * @UniqueEntity("email")
 */

class Subscribe
{
    /**
     * Subscribe ID
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Subscribe email address
     *
     * @ORM\Column(type="string", length=255, unique=true)
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * Subscribe token
     *
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * Is affiliate active
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $active;

    /**
     * The last seller date
     *
     * @ORM\Column(type="datetime")
     */
    private $lastSeller;

    /**
     * To which categories is the affiliate subscribed
     *
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="subscribes")
     * @ORM\JoinTable(
     *     joinColumns={@ORM\JoinColumn(referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(referencedColumnName="id")},
     * )
     */
    private $categories;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank()
     * @Assert\Choice(callback = "getPeriodicityValues")
     */
    private $periodicity;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new ArrayCollection();

        $this->lastSeller = new DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Subscribe
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @ORM\PrePersist()
     *
     * @return Subscribe
     */
    public function setToken($token = null)
    {
        if (!$token)
        {
            $this->token = $token;
            /** @var Subscribe $this */
            return $this;
        }
        elseif (!$this->getToken()) {
            $token = sha1($this->getEmail() . rand(11111, 99999));
            $this->token = $token;
        }

        /** @var Subscribe $this */
        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Subscribe
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    public function isActive()
    {
        return $this->active;
    }

    /**
     * Activate the affiliate
     *
     * @return boolean
     */
    public function activate()
    {
        if (!$this->isActive()) {
            $this->setActive(true);
        }

        return $this->active;
    }

    /**
     * Deactivate the affiliate
     *
     * @return boolean
     */
    public function deactivate()
    {
        if ($this->isActive()) {
            $this->setActive(false);
        }

        return $this->active;
    }

    /**
     * Set lastSeller
     *
     * @param \DateTime $lastSeller
     *
     * @return Subscribe
     */
    public function setLastSeller($lastSeller)
    {
        $this->lastSeller = $lastSeller;

        return $this;
    }

    /**
     * Get lastSeller
     *
     * @return \DateTime
     */
    public function getLastSeller()
    {
        return $this->lastSeller;
    }

    /**
     * Add category
     *
     * @param \IbwJobeetBundle\Entity\Category $category
     *
     * @return Subscribe
     */
    public function addCategory(Category $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \IbwJobeetBundle\Entity\Category $category
     */
    public function removeCategory(Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Returns a list of possible periodicity values as seconds
     *
     * @return array
     */
    public static function getPeriodicityValues(){
        return array(
            'daily' => 'daily',
            'weekly' => 'weekly',
            'monthly' => 'monthly',
        );
    }

    /**
     * Returns a list of possible periodicity values as seconds
     *
     * @return array
     */
    public static function getPeriodicitySeconds()
    {
        return array(
            'daily' => 3600 * 24,
            'weekly' => 3600 * 24 * 7,
            'monthly' => 3600 * 24 * 30,
        );
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set periodicity
     *
     * @param string $periodicity
     *
     * @return Subscribe
     */
    public function setPeriodicity($periodicity)
    {
        $this->periodicity = $periodicity;

        return $this;
    }

    /**
     * Get periodicity
     *
     * @return string
     */
    public function getPeriodicity()
    {
        return $this->periodicity;
    }
}
