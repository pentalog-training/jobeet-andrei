<?php
namespace IbwJobeetBundle\Entity;

use IbwJobeetBundle\Utils\Jobeet;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="IbwJobeetBundle\Repository\CategoryRepository")
 * @ORM\Table(name="category")
 * @ORM\HasLifecycleCallbacks()
 */
class Category
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups({"category-list", "category-details"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     *
     * @Groups({"category-list", "category-details", "job-details"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=true)
     *
     * @Groups({"category-list", "category-details"})
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="Job", mappedBy="category")
     *
     * @Groups({"category-details"})
     */
    private $jobs;

    /**
     * @ORM\ManyToMany(targetEntity="Affiliate", mappedBy="categories")
     */
    private $affiliates;

    /**
     * @ORM\ManyToMany(targetEntity="Subscribe", mappedBy="categories")
     */
    private $subscribes;

    private $activeJobs;

    private $moreJobs;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->jobs = new ArrayCollection();
        $this->affiliates = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;
        $this->setSlugValue();

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add jobs
     *
     * @param Job $jobs
     * @return Category
     */
    public function addJob(Job $jobs)
    {
        $this->jobs[] = $jobs;

        return $this;
    }

    /**
     * Remove jobs
     *
     * @param Job $jobs
     */
    public function removeJob(Job $jobs)
    {
        $this->jobs->removeElement($jobs);
    }

    /**
     * Get jobs
     *
     * @return Collection
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * Add affiliates
     *
     * @param Affiliate $affiliates
     * @return Category
     */
    public function addAffiliate(Affiliate $affiliates)
    {
        $this->affiliates[] = $affiliates;

        return $this;
    }

    /**
     * Remove affiliates
     *
     * @param Affiliate $affiliates
     */
    public function removeAffiliate(Affiliate $affiliates)
    {
        $this->affiliates->removeElement($affiliates);
    }

    /**
     * Get affiliates
     *
     * @return Collection
     */
    public function getAffiliates()
    {
        return $this->affiliates;
    }

    /**
     * @param $jobs
     */
    public function setActiveJobs($jobs)
    {
        $this->activeJobs = $jobs;
    }

    /**
     * @return Job[]
     */
    public function getActiveJobs()
    {
        return $this->activeJobs;
    }

    /**
     * @param $jobs
     */
    public function setMoreJobs($jobs)
    {
        $this->moreJobs = $jobs >= 0 ? $jobs : 0;
    }

    /**
     * @return mixed
     */
    public function getMoreJobs()
    {
        return $this->moreJobs;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Category
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function setSlugValue()
    {
        $this->slug = Jobeet::slugify($this->getName());
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Add subscribe
     *
     * @param \IbwJobeetBundle\Entity\Subscribe $subscribe
     *
     * @return Category
     */
    public function addSubscribe(\IbwJobeetBundle\Entity\Subscribe $subscribe)
    {
        $this->subscribes[] = $subscribe;

        return $this;
    }

    /**
     * Remove subscribe
     *
     * @param \IbwJobeetBundle\Entity\Subscribe $subscribe
     */
    public function removeSubscribe(\IbwJobeetBundle\Entity\Subscribe $subscribe)
    {
        $this->subscribes->removeElement($subscribe);
    }

    /**
     * Get subscribes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubscribes()
    {
        return $this->subscribes;
    }
}
