<?php
namespace IbwJobeetBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Affiliate entity. Affiliates subscribe to a newsletter to receive the latest jobs on the selected categories.
 *
 * @ORM\Entity(repositoryClass="IbwJobeetBundle\Repository\AffiliateRepository")
 * @ORM\Table(name="affiliate")
 * @ORM\HasLifecycleCallbacks()
 *
 * @UniqueEntity("email")
 */
class Affiliate
{
    /**
     * Affiliate ID
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Affiliate URL
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Assert\Url()
     */
    private $url;

    /**
     * Affiliate email address
     *
     * @ORM\Column(type="string", length=255, unique=true)
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * Affiliate token
     *
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * Is affiliate active
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $active;

    /**
     * When was the affiliate created
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;


    /**
     * To which categories is the affiliate subscribed
     *
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="affiliates")
     * @ORM\JoinTable(
     *     joinColumns={@ORM\JoinColumn(referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(referencedColumnName="id")},
     * )
     */
    private $categories;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    /**
     * Set the createdAt to current date/time at PrePersist
     *
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new DateTime();
    }

    /**
     * Get affiliate id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get affiliate URL
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set affiliate URL
     *
     * @param string $url
     * @return Affiliate
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get affiliate creation date/time
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set affiliate creation date/time
     *
     * @param \DateTime $createdAt DateTime value to be set as createdAt
     * @return Affiliate
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Add categories
     *
     * @param Category $categories
     * @return Affiliate
     */
    public function addCategory(Category $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove a category
     *
     * @param Category $categories
     */
    public function removeCategory(Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Generate a random token value and set it
     *
     * @ORM\PrePersist
     *
     * @return Affiliate $this
     */
    public function setTokenValue()
    {
        if (!$this->getToken()) {
            $token = sha1($this->getEmail() . rand(11111, 99999));
            $this->token = $token;
        }

        /** @var Affiliate $this */
        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return Affiliate
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Affiliate
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Check if the affiliate is active
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Activate the affiliate
     *
     * @return boolean
     */
    public function activate()
    {
        if (!$this->isActive()) {
            $this->setActive(true);
        }

        return $this->active;
    }

    /**
     * Deactivate the affiliate
     *
     * @return boolean
     */
    public function deactivate()
    {
        if ($this->isActive()) {
            $this->setActive(false);
        }

        return $this->active;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Affiliate
     */
    public function setActive($isActive)
    {
        $this->active = $isActive;

        return $this;
    }
}
