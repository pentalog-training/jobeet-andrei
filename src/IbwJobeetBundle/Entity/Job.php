<?php
namespace IbwJobeetBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use IbwJobeetBundle\Utils\Jobeet;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\PostDeserialize;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="IbwJobeetBundle\Repository\JobRepository")
 * @ORM\Table(name="job")
 * @ORM\HasLifecycleCallbacks()
 *
 */
class Job
{
    /**
     * Company logo
     *
     * @var UploadedFile
     *
     * @Assert\Image()
     *
     * @Serializer\Groups({"job-add"})
     */
    public $file;

    /**
     * Job ID
     *
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Groups({"job-list", "job-details", "category-details"})
     */
    private $id;

    /**
     * Job type
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Assert\NotBlank()
     * @Assert\Choice(callback = "getTypeValues")
     *
     * @Serializer\Groups({"job-list", "job-details", "category-details", "job-add"})
     */
    private $type;

    /**
     * Job company
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank()
     *
     * @Serializer\Groups({"job-details"})
     */
    private $company;

    /**
     * Job logo
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Serializer\Groups({"job-details"})
     */
    private $logo;

    /**
     * Job URL
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Assert\Url()
     *
     * @Serializer\Groups({"job-list", "job-details", "category-details"})
     */
    private $url;

    /**
     * Job position
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank()
     *
     * @Serializer\Groups({"job-list", "job-details", "category-details", "job-add"})
     */
    private $position;

    /**
     * Job location
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank()
     *
     * @Serializer\Groups({"job-details"})
     */
    private $location;

    /**
     * Job description
     *
     * @ORM\Column(type="text")
     *
     * @Assert\NotBlank()
     *
     * @Serializer\Groups({"job-details"})
     */
    private $description;

    /**
     * How to apply to job
     *
     * @ORM\Column(type="text")
     *
     * @Assert\NotBlank()
     *
     */
    private $howToApply;

    /**
     * Job token
     *
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $token;

    /**
     * Whether the job is public
     *
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @Serializer\Groups({"job-list", "job-details", "category-details"})
     */
    private $public;

    /**
     * Whether the job is active
     *
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    private $active;

    /**
     * Contact email for applying to job
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     *
     * @Serializer\Groups({"job-details"})
     */
    private $email;

    /**
     * Job expiration date
     *
     * @ORM\Column(type="datetime")
     */
    private $expiresAt;

    /**
     * Date the job was created
     *
     * @ORM\Column(type="datetime")
     *
     * @Serializer\Groups({"job-details"})
     */
    private $createdAt;

    /**
     * Date the job was updated
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Serializer\Groups({"job-details"})
     */
    private $updatedAt;

    /**
     * Job category
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="jobs")
     * @ORM\JoinColumn(referencedColumnName="id")
     *
     * @Assert\NotBlank()
     *
     * @Serializer\Groups({"job-details"})
     */
    private $category;

    /**
     * Job constructor.
     *
     * @PostDeserialize
     */
    public function __construct()
    {
        if (empty($this->token)) {
            $this->setTokenValue();
        }

        $this->createdAt = new DateTime();
        $this->updatedAt = new DateTime();
        $this->expiresAt = new DateTime('+30days');
    }

    /**
     * @ORM\PrePersist()
     */
    public function setTokenValue()
    {
        if (!$this->getToken()) {
            $this->token = sha1($this->getEmail() . rand(11111, 99999));
        }
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return Job
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Job
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return array
     */
    public static function getTypeValues()
    {
        return array_keys(self::getTypes());
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return array(
            'full-time' => 'Full time',
            'part-time' => 'Part time',
            'freelance' => 'Freelance',
        );
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        if (!$this->getCreatedAt()) {
            $this->createdAt = new DateTime();
        }

        $this->setUpdatedAt(new DateTime());
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Job
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updatedAt = new DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isPublic
     *
     * @param boolean $public
     * @return Job
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Is public
     *
     * @return boolean
     */
    public function isPublic()
    {
        return $this->public;
    }

    /**
     * Is active
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Job
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompanySlug()
    {
        return Jobeet::slugify($this->getCompany());
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return Job
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return string
     */
    public function getPositionSlug()
    {
        return Jobeet::slugify($this->getPosition());
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return Job
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocationSlug()
    {
        return Jobeet::slugify($this->getLocation());
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return Job
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getWebPath()
    {
        return null === $this->logo ? null : $this->getUploadDir() . '/' . $this->logo;
    }

    /**
     * @return string
     */
    public function getUploadDir()
    {
        return 'uploads/jobs';
    }

    /**
     * @ORM\PrePersist
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            $this->logo = uniqid() . '.' . $this->file->guessExtension();
        }
    }

    /**
     * ORM\PostPersist
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        // If there is an error when moving the file, and exception will be automatically
        // thrown by move(). This will properly prevent the entity from being persisted to
        // the database on error
        $this->file->move($this->getUploadRootDir(), $this->logo);

        unset($this->file);
    }

    /**
     * @return string
     */
    protected function getUploadRootDir()
    {
        return '%kernel.root_dir%/../web' . $this->getUploadDir();
    }

    /**
     * @ORM\PostRemove
     */
    public function removeUpload()
    {
        if (is_readable($this->file)) {
            if ($file = $this->getAbsolutePath()) {
                unlink($file);
            }
        }
    }

    /**
     * @return null|string
     */
    public function getAbsolutePath()
    {
        return null === $this->logo ? null : $this->getUploadRootDir() . '/' . $this->logo;
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        return $this->getDaysBeforeExpires() < 0;
    }

    /**
     * @return float
     */
    public function getDaysBeforeExpires()
    {
        return ceil(($this->getExpiresAt()->format('U') - time()) / 86400);
    }

    /**
     * Get expiresAt
     *
     * @return \DateTime
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * @param DateTime $date
     */
    public function setExpiresAt(DateTime $date)
    {
        $this->expiresAt = $date;
    }

    /**
     * Activate the job
     */
    public function publish()
    {
        $this->setActive(true);
    }

    /**
     * Set setActive
     *
     * @param boolean $active
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return bool
     */
    public function extend()
    {
        if (!$this->expiresSoon()) {
            return false;
        }

        $this->expiresAt = new DateTime(date('Y-m-d H:i:s', time() + 86400 * 30));

        return true;
    }

    /**
     * @return bool
     */
    public function expiresSoon()
    {
        return $this->getDaysBeforeExpires() < 5;
    }

    /**
     * @param $host
     * @return array
     */
    public function asArray($host)
    {
        return array(
            'category' => $this->getCategory()->getName(),
            'type' => $this->getType(),
            'company' => $this->getCompany(),
            'logo' => $this->getLogo() ? 'http://' . $host . '/uploads/jobs/' . $this->getLogo() : null,
            'url' => $this->getUrl(),
            'position' => $this->getPosition(),
            'location' => $this->getLocation(),
            'description' => $this->getDescription(),
            'how_to_apply' => $this->getHowToApply(),
            'expiresAt' => $this->getCreatedAt()->format('Y-m-d H:i:s'),
        );
    }

    /**
     * Get category
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set category
     *
     * @param Category $category
     * @return Job
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Job
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return Job
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Job
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Job
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get howToApply
     *
     * @return string
     */
    public function getHowToApply()
    {
        return $this->howToApply;
    }

    /**
     * Set howToApply
     *
     * @param string $howToApply
     * @return Job
     */
    public function setHowToApply($howToApply)
    {
        $this->howToApply = $howToApply;

        return $this;
    }

    /**
     * Get public
     *
     * @return boolean
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
}
