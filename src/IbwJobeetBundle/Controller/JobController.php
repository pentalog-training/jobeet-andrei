<?php
namespace IbwJobeetBundle\Controller;

use IbwJobeetBundle\Entity\Category;
use IbwJobeetBundle\Entity\Job;
use IbwJobeetBundle\Form\Type\JobType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/{_locale}/job", requirements={"_locale": "en|fr"})
 */
class JobController extends Controller
{
    /**
     * @Route("/", name="job_index", defaults={"_locale" = "en"})
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        if($request->get('_route') == 'jobeet_homepage_nonlocalized') {
            return $this->redirect($this->generateUrl('jobeet_homepage'));
        }

        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('IbwJobeetBundle:Category')->getWithJobs();

        foreach ($categories as $category) {
            /** @var Category $category */
            $category->setActiveJobs($em->getRepository('IbwJobeetBundle:Job')->getActiveJobs(
                $category->getId(),
                $this->container->getParameter('max_jobs_on_homepage')
            ));

            $activeJobs = $em->getRepository('IbwJobeetBundle:Job')->countActiveJobs($category->getId());
            $category->setMoreJobs(
                $activeJobs - $this->container->getParameter('max_jobs_on_homepage')
            );
        }

        $latestJob = $em->getRepository('IbwJobeetBundle:Job')->getLatestPost();

        if($latestJob) {
            $lastUpdated = $latestJob->getCreatedAt()->format(DATE_ATOM);
        } else {
            $lastUpdated = new \DateTime();
            $lastUpdated = $lastUpdated->format(DATE_ATOM);
        }
        
        $format = $request->getRequestFormat();
        return $this->render(':job:index.'.$format.'.twig', array(
            'categories' => $categories,
            'lastUpdated' => $lastUpdated,
            'feedId' => sha1($this->get('router')->generate('job_index', array('_format'=> 'atom'), true)),
        ));
    }

    /**
     * Finds and displays a Job entity.
     *
     * @Route("/{company}/{location}/{id}/{position}/{modal}", name="job_show", requirements={"id": "\d+", "_locale": "en|fr"}, defaults={"modal" = ""})
     * @Method("GET")
     * @param integer $id
     * @param boolean $modal Whether the job description will be displayed in a modal window
     * @param Request $request
     * @return Response
     */
    public function showAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IbwJobeetBundle:Job')->getActiveJob($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find job entry');
        }

        $session = $request->getSession();

        // fetch jobs already stored in the job history
        $jobs = $session->get('job_history', array());

        // store the job as an array so we can put it in the session and avoid entity serialize errors
        $job = array('id' => $entity->getId(), 'position' =>$entity->getPosition(), 'company' => $entity->getCompany(), 'companyslug' => $entity->getCompanySlug(), 'locationslug' => $entity->getLocationSlug(), 'positionslug' => $entity->getPositionSlug());

        if (!in_array($job, $jobs)) {
            // add the current job at the beginning of the array
            array_unshift($jobs, $job);

            // store the new job history back into the session
            $session->set('job_history', array_slice($jobs, 0, 3));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render(':job:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a new Job entity.
     *
     * @Route("/new", name="job_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $entity = new Job();
        $entity->setType('full-time');
        $form = $this->createForm(new JobType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->publish();

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('job_preview', array(
                'company' => $entity->getCompanySlug(),
                'location' => $entity->getLocationSlug(),
                'token' => $entity->getToken(),
                'position' => $entity->getPositionSlug(),
            )));
        }

        return $this->render(':job:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Edits a Job entity
     *
     * @Route("/{token}/edit", name="job_edit", requirements={"_locale": "en|fr"})
     * @param $token
     * @return Response
     */
    public function editAction($token)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Job $entity */
        $entity = $em->getRepository('IbwJobeetBundle:Job')->findOneByToken(array('token' => $token));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Job entity.');
        }

        if (!$entity->isActive()) {
            throw $this->createNotFoundException('Job is activated and cannot be edited.');
        }

        // ***********************
        dump($entity->isActive());
        //************************

        $editForm = $this->createForm(new JobType(), $entity);

        return $this->render(':job:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * @Route("/{token}/extend", name="job_extend", requirements={"_locale": "en|fr"})
     * @Method("POST")
     * @param Request $request
     * @param $token
     * @return RedirectResponse
     */
    public function extendAction($token, Request $request)
    {
        $form = $this->createExtendForm($token);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            /** @var Job $entity */
            $entity = $em->getRepository('IbwJobeetBundle:Job')->findOneByToken($token);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Job entity.');
            }

            if (!$entity->extend()) {
                throw $this->createNotFoundException('Unable to extend the Job');
            }

            $em->persist($entity);
            $em->flush();

            $this->get('session')
                ->getFlashBag()
                ->add(
                    'notice',
                    sprintf(
                        'Your job validity has been extended until %s',
                        $entity->getExpiresAt()->format('m/d/Y')
                    )
                );
        }

        return $this->redirect($this->generateUrl('job_preview', array(
            'company' => $entity->getCompanySlug(),
            'location' => $entity->getLocationSlug(),
            'token' => $entity->getToken(),
            'position' => $entity->getPositionSlug(),
        )));
    }

    /**
     * @Route("/{token}/update", name="job_update", requirements={"_locale": "en|fr"})
     *
     * @param Request $request
     * @param $token
     *
     * @return RedirectResponse|Response
     */
    public function updateAction(Request $request, $token)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IbwJobeetBundle:Job')->findOneByToken(array('token' => $token));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Job entity');
        }

        $editForm = $this->createForm(new JobType(), $entity);

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('job_preview', array(
                'company' => $entity->getCompanySlug(),
                'location' => $entity->getLocationSlug(),
                'token' => $entity->getToken(),
                'position' => $entity->getPositionSlug(),
            )));
        }

        return $this->render(':job:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * @Route("/{token}/delete", name = "job_delete", requirements={"_locale": "en|fr"})
     *
     * @param Request $request
     * @param $token
     *
     * @return RedirectResponse
     */
    public function deleteAction($token)
    {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('IbwJobeetBundle:Job')->findOneByToken(array('token' => $token));

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Job entity.');
            }

            $em->remove($entity);
            $em->flush();

        return $this->redirect($this->generateUrl('job_index'));
    }

    /**
     * @Route("/{company}/{location}/{token}/{position}", name="job_preview", requirements={"token": "\w+"})
     * @param $token
     * @return Response
     */
    public function previewAction($token)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IbwJobeetBundle:Job')->findOneByToken($token);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Job entity.');
        }

//        $deleteForm = $this->createDeleteForm($entity->getId());
//        $publishForm = $this->createPublishForm($entity->getToken());
//        $extendForm = $this->createExtendForm($entity->getToken());

        return $this->render(':job:show.html.twig', array(
            'entity'      => $entity,
//            'delete_form' => $deleteForm->createView(),
//            'publish_form' => $publishForm->createView(),
//            'extend_form' => $extendForm->createView(),
        ));
    }

    /**
     * @Route("/{token}/publish", name="job_publish", requirements={"_locale": "en|fr"})
     * @Method("POST")
     *
     * @param Request $request
     * @param $token
     * @return RedirectResponse
     */
    public function publishAction(Request $request, $token)
    {
        $form = $this->createPublishForm($token);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('IbwJobeetBundle:Job')->findOneByToken(array('token' => $token));

        if ($form->isValid()) {

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Job entity.');
            }

            $entity->publish();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Your job is now online for 30 days.');
        }

        return $this->redirect($this->generateUrl('job_preview', array(
            'company'   => $entity->getCompanySlug(),
            'location'  => $entity->getLocationSlug(),
            'token'     => $entity->getToken(),
            'position'  => $entity->getPositionSlug()
        )));
    }

    private function createDeleteForm($token)
    {
        return $this->createFormBuilder(array('token' => $token))
            ->add('token', HiddenType::class)
            ->getForm()
            ;
    }

    private function createPublishForm($token)
    {
        return $this->createFormBuilder(array('token' => $token))
            ->add('token', HiddenType::class)
            ->getForm()
            ;
    }

    private function createExtendForm($token)
    {
        return $this->createFormBuilder(array('token' => $token))
            ->add('token', 'hidden')
            ->getForm();
    }
}
