<?php
namespace IbwJobeetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SearchController
 * @package AppBundle\Controller
 */
class SearchController extends Controller
{
    /**
     * @Route("/{_locale}/job/search/{page}", name="job_search", defaults={"page" : 1}, requirements={"_locale": "en|fr"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function searchAction($page, Request $request)
    {
        // get the query string
        $q = $request->get('keywords');

        // redirect to the main page if no query found
        if (!$q) {
            if (!$request->isXmlHttpRequest()) {
                return $this->redirect($this->generateUrl('job_index'));
            } else {
                return new Response('No results.');
            }
        }

        $query = $this->get('app.repository.job')->searchForJob($q);
        $jobs = $query->getArrayResult();
        
        $total_jobs = count($jobs);
        $jobs_per_page = $this->container->getParameter('max_jobs_on_search');
        $last_page = ceil($total_jobs / $jobs_per_page);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;
        $from = ($page - 1) * $jobs_per_page;
        $jobs = array_slice($jobs, $from, $jobs_per_page);

        $pageInfo = [
            'jobs' => $jobs,
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'total_jobs' => $total_jobs
        ];

        if ($request->isXmlHttpRequest()) {
            if('*' == $query || !$jobs || $query == '') {
                return new Response('No results.');
            }

            return $this->render(':job:list.html.twig', array(
                'jobs' => $jobs,
                'keyword' => $q,
            ));
        }

        return $this->render(':job:search.html.twig', array(
            'pageInfo' => $pageInfo,
            'keyword' => $q,
        ));
    }
}
