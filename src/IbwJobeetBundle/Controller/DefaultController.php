<?php

namespace IbwJobeetBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/{_locale}", name="jobeet_homepage", requirements={"_locale": "en|fr"})
     * @Route("/", name="jobeet_homepage_nonlocalized")
     */
    public function indexAction(){
        return $this->redirect($this->generateUrl('job_index'));
    }

    /**
     * @Route("/login", name="login")
     * @param Request $request
     * @return Response Response
     */
    public function loginAction(Request $request)
    {
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(Security::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(Security::AUTHENTICATION_ERROR);
            $session->remove(Security::AUTHENTICATION_ERROR);
        }

        return $this->render(':default:login.html.twig', array(
            // last username entered by the user
            'last_username' => $session->get(Security::LAST_USERNAME),
            'error'         => $error,
        ));
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {
    }

    /**
     * @Route("/login_check", name="login_check")
     */
    public function loginCheckAction()
    {
    }

    /**
     * @Route("/change_language", name="change_language")
     * @param Request $request
     * @return RedirectResponse
     */
    public function changeLanguageAction(Request $request)
    {
        $language = $request->query->get("language");

        return $this->redirect($this->generateUrl("job_index", array('_locale' => $language)));
    }
    
}
