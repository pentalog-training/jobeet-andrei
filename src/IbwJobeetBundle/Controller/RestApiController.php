<?php
namespace IbwJobeetBundle\Controller;

use IbwJobeetBundle\Entity\Category;
use IbwJobeetBundle\Entity\Job;
use IbwJobeetBundle\Form\DataTransformer\NumberToCategoryTransformer;
use JMS\Serializer\SerializationContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;

/**
 * Class RestApiController
 *
 * @package IbwJobeetBundle\Controller
 *
 * @Route("/restapi")
 */
class RestApiController extends Controller
{
    /**
     * Get all
     *
     * @Route("/jobs", name="restapi_job")
     * @Method("GET")
     * @return Response Returns data as JSON
     */
    public function getJobs()
    {
        $jobsRepository = $this->container->get('app.repository.job');
        $jobs = $jobsRepository->findAll();

        $serializer = $this->container->get('jms_serializer');
        $json = $serializer->serialize(
            $jobs,
            'json',
            SerializationContext::create()->setGroups(['job-list'])
        );

        return new Response($json, 200, ['Content-Type' => 'text/json']);
    }

    /**
     * Get one job by ID
     *
     * @Route("/job/{job}")
     * @Method("GET")
     *
     * @param Job/int $job Job ID
     * @ParamConverter("job", class="IbwJobeetBundle:Job")
     *
     * @return Response
     */
    public function getJob(Job $job)
    {
        $serializer = $this->container->get('jms_serializer');
        $json = $serializer->serialize(
            $job,
            'json',
            SerializationContext::create()->setGroups(['job-details'])
        );

        return new Response($json, 200, ['Content-Type' => 'text/json']);
    }

    /**
     * Get all categories
     *
     * @Route("/categories")
     * @Method("GET")
     */
    public function getCategories()
    {
        $categoryRepository = $this->container->get('app.repository.category');
        $categories = $categoryRepository->getCategories();

        $serializer = $this->container->get('jms_serializer');
        $json = $serializer->serialize(
            $categories,
            'json',
            SerializationContext::create()->setGroups(['category-list'])
        );

        return new Response($json, 200, ['Content-Type' => 'text/json']);
    }

    /**
     * Get one category by ID
     *
     * @Route("/category/{category}")
     * @Method("GET")
     *
     * @param Category|int $category Category ID
     * @ParamConverter("category", class="IbwJobeetBundle:Category")
     *
     * @return Response
     */
    public function getCategory(Category $category)
    {
        $serializer = $this->container->get('jms_serializer');
        $json = $serializer->serialize(
            $category,
            'json',
            SerializationContext::create()->setGroups(['category-details'])
        );

        return new Response($json, 200, ['Content-Type' => 'text/json']);
    }

    /**
     * Add a new job
     *
     * @Route("/job/add")
     * @method("POST")
     *
     * @param Request $request
     *
     * @return Response Returns the ID of the added job
     */
    public function addJob(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $serializer = $this->container->get('jms_serializer');
        $validator = $this->get('validator');

        $jobJson = $request->get('job');
        $categoryId = $request->get('category');

        $jobString = json_encode($jobJson);

        /** @var Job $job */
        $job = $serializer->deserialize($jobString, 'IbwJobeetBundle\Entity\Job', 'json');
        /** @var Category $category */
        $category = $this->getDoctrine()->getRepository('IbwJobeetBundle:Category')->find($categoryId);

        $job->setCategory($category);

        $errors = $validator->validate($job);

        if(count($errors) > 0)
        {
            return new Response($errors);
        }
        else
        {
            $em->persist($job);
            $em->flush();
        }

        return new JsonResponse(
            ['success' => true , $job],
            201
        );
    }

    /**
     * Add a new category
     *
     * @Route("/category/add")
     * @Method("POST")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function addCategory(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $serializer = $this->container->get('jms_serializer');
        $validator = $this->get('validator');

        /** @var Category $category */
        $category = $serializer->deserialize($request->getContent(), 'IbwJobeetBundle\Entity\Category', 'json');

        $errors = $validator->validate($category);

        if(count($errors) > 0)
        {
            return new Response($errors);
        }
        else
        {
            $em->persist($category);
            $em->flush();
        }

        return new JsonResponse(
            ['success' => true],
            201
        );
    }

    /**
     * Delete job by ID
     *
     * @Route("/job/delete/{job}")
     * @Method("DELETE")
     *
     * @param Job|int $job Job ID of the job that will be deleted
     * @ParamConverter("job", class="IbwJobeetBundle:Job")
     *
     * @return JsonResponse
     */
    public function deleteJob(Job $job)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($job);
        $em->flush();

        return new JsonResponse(
            ['success' => true],
            200
        );
    }

    /**
     * Delete category by ID
     *
     * @Route("/category/delete/{category}")
     * @Method("DELETE")
     *
     * @param Category|int $category Category ID of the category that will be deleted
     * @ParamConverter("category", class="IbwJobeetBundle:Category")
     *
     * @return JsonResponse
     */
    public function deleteCategory(Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();

        return new JsonResponse(
            ['success' => true],
            200
        );
    }

    /**
     * Update job
     *
     * @Route("/job/update")
     * @method("PUT")
     *
     * @param Request $request
     *
     * @return Response Return the ID of the updated job
     */
    public function updateJob(Request $request)
    {
        /** @var Serializer $serializer */
        $serializer = $this->container->get('jms_serializer');
        $em = $this->getDoctrine()->getManager();
        $validator = $this->get('validator');

        $jobJson = $request->get('job');
        $categoryId = $request->get('category');

        $jobString = json_encode($jobJson);

        /** @var Job $job */
        $job = $serializer->deserialize($jobString, 'IbwJobeetBundle\Entity\Job', 'json');
        $category = $this->getDoctrine()->getRepository('IbwJobeetBundle:Category')->find($categoryId);

        $job->setCategory($category);

        $errors = $validator->validate($job);

        if(count($errors) > 0)
        {
            return new Response($errors);
        }
        else
        {
            $em->merge($job);
            $em->flush();
        }

        return new JsonResponse(
            ['success' => true, $job, $categoryId],
            200
        );
    }

    /**
     * Update category
     *
     * @Route("/category/update")
     * @method("PUT")
     *
     * @param Request $request
     *
     * @return Response Return the ID of the updated category
     */
    public function updateCategory(Request $request)
    {
        /** @var Serializer $serializer */
        $serializer = $this->container->get('jms_serializer');

        /** @var Category $category */
        $category = $serializer->deserialize($request->getContent(), 'IbwJobeetBundle\Entity\Category', 'json');

        $em = $this->getDoctrine()->getManager();
        $em->merge($category);
        $em->flush();

        return new JsonResponse(
            ['success' => true],
            200
        );
    }

    /**
     * Update category
     *
     * @Route("/test")
     * @method("POST")
     *
     * @param Request $request
     *
     */
    public function test(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $transformer = new NumberToCategoryTransformer($this->getDoctrine()->getManager());
        /** @var Serializer $serializer */
        $serializer = $this->container->get('jms_serializer');
        $data = $request->getContent();
        $data = json_decode($data);
        $data->category = json_decode($serializer->serialize($transformer->transform($data->category), 'json'));
        $data = json_encode($data);

        /** @var Job $job */
        $job = $serializer->deserialize($data, 'IbwJobeetBundle\Entity\Job', 'json');


        $em->persist($job);
        $em->flush();

        return new JsonResponse(
            ['succes' => true],
            200

        );
    }
}
