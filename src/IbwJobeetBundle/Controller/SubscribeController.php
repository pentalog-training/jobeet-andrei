<?php

namespace IbwJobeetBundle\Controller;

use IbwJobeetBundle\Entity\Subscribe;
use IbwJobeetBundle\Form\Type\SubscribeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;



/**
 * @Route("/{_locale}/subscribe")
 */
class SubscribeController extends Controller
{
    /**
     * @Route("/new", name="subscribe_new")
     */
    public function newAction()
    {
        $entity = new Subscribe();
        $form = $this->createForm(new SubscribeType(), $entity);

        return $this->render(':subscribe:subscribe_new.html.twig', array(
            'form'   => $form->createView(),
        ));
    }

    /**
     * @Route("/create", name="subscribe_create")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        $subscribe = new Subscribe();
        $form = $this->createForm(new SubscribeType(), $subscribe);
        $form->submit($request);
        $em = $this->getDoctrine()->getManager();

        if ($form->isValid()) {
            $subscribe->setActive(true);

            $em->persist($subscribe);
            $em->flush();

            return $this->redirect($this->generateUrl('subscribe_wait'));
        }

        return $this->render(':subscribe:subscribe_new.html.twig', array(
            'entity' => $subscribe,
            'form'   => $form->createView(),
        ));
    }

    /**
     * @Route("/wait", name="subscribe_wait")
     */
    public function waitAction()
    {
        return $this->render(':subscribe:wait.html.twig');
    }
}
