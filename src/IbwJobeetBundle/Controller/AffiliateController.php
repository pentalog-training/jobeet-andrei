<?php

namespace IbwJobeetBundle\Controller;

use IbwJobeetBundle\Entity\Affiliate;
use IbwJobeetBundle\Form\Type\AffiliateType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;



/**
 * @Route("/{_locale}/affiliate")
 */
class AffiliateController extends Controller
{
    /**
     * @Route("/new", name="affiliate_new")
     */
    public function newAction()
    {
        $entity = new Affiliate();
        $form = $this->createForm(new AffiliateType(), $entity);
        
        return $this->render(':affiliate:affiliate_new.html.twig', array(
            'form'   => $form->createView(),
        ));
    }

    /**
     * @Route("/create", name="affiliate_create")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        $affiliate = new Affiliate();
        $form = $this->createForm(new AffiliateType(), $affiliate);
        $form->submit($request);
        $em = $this->getDoctrine()->getManager();

        if ($form->isValid()) {
            $affiliate->setActive(false);

            $em->persist($affiliate);
            $em->flush();

            return $this->redirect($this->generateUrl('affiliate_wait'));
        }

        return $this->render(':affiliate:affiliate_new.html.twig', array(
            'entity' => $affiliate,
            'form'   => $form->createView(),
        ));
    }

    /**
     * @Route("/wait", name="affiliate_wait")
     */
    public function waitAction()
    {
        return $this->render(':affiliate:wait.html.twig');
    }
}
