<?php
namespace IbwJobeetBundle\Command;

use IbwJobeetBundle\Entity\Category;
use IbwJobeetBundle\Entity\Subscribe;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
/**
 * Class SendNewsletterCommand
 * @package IbwJobeetBundle\Command
 */
class SendNewsletterCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:newsletter:send')
            ->setDescription('Send newsletters to subscribers');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $periodicitySeconds = Subscribe::getPeriodicitySeconds();

        $mailer = $this->getContainer()->get('mailer');
//        $logger = new \Swift_Plugins_Loggers_EchoLogger();
//        $mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));

        $subscriberRepository = $this->getContainer()->get('app.repository.subscriber');
        /** @var Subscribe $subscribers */
        $subscribers = $subscriberRepository->getAllSubscribers();
        $mailCount =0;
        /** @var Subscribe $subscriber */
        foreach ($subscribers as $subscriber)
        {
            $periodicity = $subscriber->getPeriodicity();
            $lastSeller = $subscriber->getLastSeller()->format('Y-m-d H:i:s');
            /** @var \DateTime $fromDate */
            $fromDate = date('Y-m-d H:i:s', time() - $periodicitySeconds[$periodicity]);

            if ($fromDate > $lastSeller){
                $categories = $subscriber->getCategories();
                
                $jobRepository = $this->getContainer()->get('app.repository.job');
                /** @var Category $category */
                foreach ($categories as $category)
                {
                    $jobs[$category->__toString()] = $jobRepository->getLastNJobs($fromDate, $category);
                }

                $message = Swift_Message::newInstance()
                    ->setSubject('Jobeet Newsletter')
                    ->setFrom('jnl_no_reply@jobeet.local')
                    ->setTo($subscriber->getEmail())
                    ->setBody(
                        $this->getContainer()->get('templating')->render(':subscribe:newsletter.html.twig', array(

                            'jobs' => $jobs,
                        )),
                        'text/html'
                    );
                $mailer->send($message);
                $mailCount++;
            }

            $subscriber->setLastSeller(new \DateTime());
            $em = $this->getContainer()->get('doctrine')->getManager();
            $em->persist($subscriber);
            $em->flush();
//            $subscriberRepository->updateLastSeller($subscriber->getId());
        }
//        $output->writeln($logger->dump());
        $output->writeln($mailCount . ' sent messages.');
    }
}
